import styles from '../styles/Navbar.module.css'
const Navbar = () => {
    return (
        <nav className={styles.navbar}>
            <h3>HTSP.GG</h3>
            <ul className={styles.navLinks}>
                <li>
                    <a href="#">Dashboard</a>
                </li>
                <li>
                    <a href="#">Manajemen Pengguna</a>
                </li>
                <li>
                    <a href="#">Bantuan</a>
                </li>
            </ul>
            <input className={styles.searchInput}></input>
            <div>
                <h2>yow</h2>
            </div>
        </nav>
    );
}
export default Navbar;