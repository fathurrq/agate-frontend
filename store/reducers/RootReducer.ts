import {combineReducers} from "redux"
import activitiesReducer from "./ActivitiesReducer";
const rootReducer = combineReducers({
    activities : activitiesReducer
})

export default rootReducer;